package main

import (
	"partyanimal-server/framework/file"
	"partyanimal-server/framework/logger"
	"partyanimal-server/framework/net/tcp"
	"partyanimal-server/framework/net/udp"
	"partyanimal-server/handler"
	"sync"
	"time"
)

type GameConfig struct {
	AppId string 		`json:"appId"`
	UdpAddr string		`json:"udpAddr"`
	TcpAddr string		`json:"tcpAddr"`
}

func main() {
	dateStr := time.Now().Format("2006-01-02 15:04:05")
	logger.Debug("server start %s", dateStr)
	logger.Info("server start %s", dateStr)
	logger.Warn("server start %s", dateStr)
	logger.Error("server start %s", dateStr)
	logger.Net.Debug("server start %s", dateStr)
	logger.Battle.Debug("server start %s", dateStr)
	logger.Job.Debug("server start %s", dateStr)

	var gameConfig GameConfig
	file.LoadJsonConfiguration("config/game.json", &gameConfig)
	logger.Debug(gameConfig)
	//var j = job.NewJob("test")
	//
	//var i = 0
	//j.Run(time.Second, func(j *job.Job) {
	//	i++
	//	logger.Job.Info("job run %d", i)
	//	if i == 4 {
	//		j.Stop()
	//	}
	//})
	//user, err := redis.HGet("PartyAnimal:hash:role", "428323bdf6868d3902cf785a4e26709a17b0b4b4")
	//logger.Debug(user, err)
	//
	//key := []string{"428323bdf6868d3902cf785a4e26709a17b0b4b4", "1231231", "xxxxx"}
	//data, err := redis.HMGet("PartyAnimal:hash:role", key)
	//logger.Debug(data, err)
	//logger.Debug(data[2].(string))
	// 30帧
	// 开启udp服务
	var wg sync.WaitGroup
	wg.Add(1)
	go udp.Listen(gameConfig.UdpAddr, handler.Handler)
	go tcp.Listen(gameConfig.TcpAddr, handler.TCPHandler)
	wg.Wait()
}