package db

import (
	"bytes"
	"context"
	"github.com/go-redis/redis"
	"partyanimal-server/framework/file"
	"partyanimal-server/framework/logger"
	_redis "partyanimal-server/framework/redis"
	"time"
)

var rds = _redis.Client()

var ctx = context.Background()

const AppId = ""

const (
	RedisHashMutex = "hash:mutex"		// 锁
	RedisHashIncr = "hash:incr"			// 增量
	RedisZSet = "zset"					// 有序集
	RedisHashRole = "hash:role"			// 玩家数据
	RedisHashBattle = "hash:battle"		// 战斗数据

	RedisHashRoleName = "hash:role:name"			// 玩家数据
	RedisHashRoleIcon = "hash:role:icon"			// 玩家数据

)

type GameConfig struct {
	AppId string 		`json:"appId"`
}

var gameConfig GameConfig

func init() {
	file.LoadJsonConfiguration("config/game.json", &gameConfig)
	//logger.Debug(gameConfig)
	logger.Info("db init success")
}

func GenerateKey(args ...string) string {
	var key = bytes.Buffer{}
	key.WriteString(gameConfig.AppId)			// 自动前置添加appId
	for _, arg := range args {
		if arg == "" {
			continue
		}
		key.WriteString(":")
		key.WriteString(arg)
	}
	return key.String()
}

const LockDuration = 1 * time.Second

// 上锁
// key：锁分组名称
// id：锁名称
func Lock(key string, id string) bool {

	//if userId == "" {
	//	userId = "unknown"
	//}
	lockKey := GenerateKey(RedisHashMutex, key)

	lock, err := rds.SetNX(ctx, lockKey, id, LockDuration).Result()
	if err != nil {
		logger.Error("%s %s lock rds.SetNX error: %v", lockKey, id, err)
		logger.Redis.Error("%s %s lock rds.SetNX error: %v", lockKey, id, err)
		return false
	}
	//logger.Debug("%s %s lock %v", lockKey, userId, lock)
	return lock
}

// 解锁
// key：锁分组名称
// id：锁名称
func Unlock(key string, id string) bool {
	lockKey := GenerateKey(RedisHashMutex, key)
	//logger.Debug("%s %s Unlock ", lockKey, userId)
	_id, err := rds.Get(ctx, lockKey).Result()
	if err != nil {
		logger.Error("%s %s unlock rds.Get error: %v", lockKey, id, err)
		logger.Redis.Error("%s %s unlock rds.Get error: %v", lockKey, id, err)
		return false
	}
	if _id != id {
		logger.Error("%s %s unlock error: no access", lockKey, id)
		logger.Redis.Error("%s %s unlock error: no access", lockKey, id)
		return false
	}
	num, err := rds.Del(ctx, lockKey).Result()
	if err != nil {
		logger.Error("%s %s unlock rds.Del error: %v", lockKey, id, err)
		logger.Redis.Error("%s %s unlock rds.Del error: %v", lockKey, id, err)
		return false
	}
	return num > 0
}

// 增加 数值加算
// key 自增项名称
// v 增量
func Incr(key string, value int64) int64 {

	strKey := GenerateKey(RedisHashIncr)

	num, err := rds.HIncrBy(ctx, strKey, key, value).Result()
	//logger.Info("rds.HIncrBy num = %d, error = %v",num, err)
	if err != nil {
		logger.Error("Incr rds.HIncrBy error = %v", err)
		return -1
	}
	return num
}

// 添加有序集
// 更新索引key
func ZSetAdd(key string, member string, score float64) int64 {

	strKey := GenerateKey(RedisZSet, key)
	z := redis.Z{Score: score, Member: member}
	num, err := rds.ZAdd(ctx, strKey, &z).Result()
	if err != nil {
		logger.Error("ZSetAdd redis.ZAdd error = %v", err)
		return -1
	}
	return num
}

// 按排序范围获取数据
func ZSetRange(key string, start int64, stop int64) *[]string {

	strKey := GenerateKey(RedisZSet, key)
	strs, err := rds.ZRange(ctx, strKey, start, stop).Result()
	if err != nil {
		logger.Error("ZSetRange redis.ZRange error = %v", err)
		return nil
	}
	return &strs
}

//func ZSetRange2(key string, start int64, stop int64) *[]string {
//
//	strKey := GenerateKey(RedisZSet, key)
//	opt := redis.ZRangeBy{Min: start, Max: stop, }
//	strs, err := rds.ZRangeByScore(ctx, strKey, &opt).Result()
//	if err != nil {
//		logger.Error("ZSetRange redis.ZRange error = %v", err)
//		return nil
//	}
//	return &strs
//}

//获取排名
func ZSetRank(key string, member string) int64 {

	strKey := GenerateKey(RedisZSet, key)
	rank, err := rds.ZRank(ctx, strKey, member).Result()
	if err != nil {
		logger.Error("ZSetRank redis.ZRank error = %v", err)
		return -1
	}
	//logger.Debug("setdur v = %d t = %d n = %s", v, t, num)
	return rank
}

//获取排名依据分值
func ZSetScore(k string, m string) float64 {

	strKey := GenerateKey(RedisZSet, k)
	num, err := rds.ZScore(ctx, strKey, m).Result()
	if err != nil {
		logger.Error("ZSetAdd redis.ZAdd error = %v", err)
		return -1
	}
	//logger.Debug("setdur v = %d t = %d n = %s", v, t, num)
	return num
}