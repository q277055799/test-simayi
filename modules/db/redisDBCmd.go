package db

import (
	"partyanimal-server/framework/logger"
	fredis "partyanimal-server/framework/redis"
	"strconv"
)

// 保存玩家信息
func SetRole(uid string, user interface{}) error {
	key := GenerateKey(RedisHashRole)
	logger.Redis.Debug("SetRole", key, uid, user)
	return fredis.HSetJson(key, uid, user)
}

// 获取玩家数据
func GetRole(uid string, user interface{}) error {
	key := GenerateKey(RedisHashRole)
	logger.Redis.Debug("GetRole %s %s", key, uid)
	return fredis.HGetJson(key, uid, user)
}

// 批量获取玩家数据
func MGetRole(uids []string, users []interface{}) error {
	key := GenerateKey(RedisHashRole)
	return fredis.HMGetJson(key, uids, users)
}

// 设置玩家名字
func SetRoleName(uid string, name string) error {
	key := GenerateKey(RedisHashRoleName)
	return fredis.HSet(key, uid, name)
}

// 获取玩家名字
func GetRoleName(uid string) (string, error) {
	key := GenerateKey(RedisHashRoleName)
	val, err := fredis.HGet(key, uid)
	var name string
	if err != nil {
		v, ok := val.(string)
		if ok {
			name = v
		} else {
			name = ""
		}
	}
	return name, err
}

// 设置玩家形象
func SetRoleIcon(uid string, icon int) error {
	key := GenerateKey(RedisHashRoleIcon)
	return fredis.HSet(key, uid, icon)
}
// 获取玩家形象
func GetRoleIcon(uid string) (int, error) {
	key := GenerateKey(RedisHashRoleIcon)
	val, err := fredis.HGet(key, uid)
	var icon int
	if err != nil {
		v, ok := val.(string)
		if ok {
			icon, _ = strconv.Atoi(v)
		}
	}
	return icon, err
}

// 保存战斗数据
func SaveBattle(battleId string, battle interface{}) error {
	key := GenerateKey(RedisHashBattle)
	return fredis.HSetJson(key, battleId, battle)
}

// 提取战斗数据
func GetBattle(battleId string, battle interface{}) error {
	key := GenerateKey(RedisHashBattle)
	return fredis.HGetJson(key, battleId, battle)
}