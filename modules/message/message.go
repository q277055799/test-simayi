package message

//type MsgId string
//
//// 消息号
//const (
//	MSG_EnterLobby     MsgId = "EnterLobbyRequest"
//	MSG_CreateRoom     MsgId = "CreateRoomRequest"
//	MSG_EnterRoom     MsgId = "EnterRoomRequest"
//	MSG_StartBattle     MsgId = "StartBattleRequest"
//	MSG_GetRrame     MsgId = "GetFrameRequest"
//)

type MsgId byte
const (
	MSG_Heartbeat  MsgId = iota // 心跳包 0
	MSG_EnterLobby              // 进入大厅 1
	MSG_CreateRoom              // 创建房间 2
	MSG_EnterRoom               // 进入房间 3
	MSG_LeaveRoom               // 离开房间 4
	MSG_CloseRoom               // 关闭房间 5

	MSG_StartBattle								// 开始战斗 6
	MSG_AddCmd									// 上传指令 7
	MSG_GetFrame								// 补帧 8
	MSG_EndBattle								// 结束战斗 9
	MSG_ReturnBattle							// 回到战斗 10
	MSG_UpdateUser								// 更新玩家信息 11
	MSG_ReadyRoom					// 准备 12
	MSG_KickRoom                	// 踢人 13

)

type Code int
const (
	CODE_OK Code = iota
	CODE_Fail
	CODE_ParamError
	CODE_ShouldEnterLobbyFirst
	CODE_NotAtRoom
	CODE_NotRoomMaster
	CODE_BattleIsOver
	CODE_CantChangeAvatarInBattle
	CODE_NotAllReady
)

type NotifyType int
const (
	Notify_CloseRoom NotifyType = iota
	Notify_LeaveRoom
	Notify_StartBattle
	Notify_EndBattle
	Notify_ReadyRoom
	Notify_KickRoom
)

type RequestBase struct {
	Uid   string `json:"Uid"`   // 用户唯一id 身份标示
	Sign  string `json:"Sign"`  // 校验码，由上一次请求返回提供
	MsgId MsgId  `json:"MsgId"` // 消息id
}

type ResponseBase struct {
	Code Code   `json:"Code"`
	Sign string `json:"Sign"`			//
	Msg  string `json:"Msg"`			// 错误信息
}

type NotifyBase struct {
	Type NotifyType `json:"NotifyType"`
	Data string     `json:"Data"`
}

//func ToJson(v interface{}) string {
//	bytes, err := json.Marshal(v)
//	if err != nil {
//		logger.Error("Message.ToJson err = %v, data = %v", err, v)
//		return ""
//	}
//	return string(bytes)
//}
