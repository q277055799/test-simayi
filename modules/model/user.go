package model

import (
	"github.com/go-redis/redis"
	"net"
	"partyanimal-server/modules/db"
)

// 性别
type Sex int
const (
	Sex_Male	Sex = 0
	Sex_Female	Sex = 1
)

type User struct {
	Uid string				`json:"Uid"`
	Name string				`json:"Name"`
	Icon int				`json:"Icon"`
	Sex	Sex					`json:"Sex"`
	Addr *net.UDPAddr		`json:"-"`	// 不暴露此字段
	RoomId int				`json:"RoomId"`	// 房间id
	LastOnlineTime int64 	`json:"LastOnlineTime"` // 最后在线时间
	State bool				`json:"State"`			// 准备标记
}

type UserMap map[string]*User


//func NewUser(uid string, name string, sex Sex) *User  {
//	return &User{Uid: uid, Name: name, Sex: sex}
//}

func LoadUser(uid string) (*User, error) {
	name, err := db.GetRoleName(uid)
	if err != nil && err != redis.Nil  {
		return nil, err
	}
	icon, err := db.GetRoleIcon(uid)
	if err != nil && err != redis.Nil {
		return nil, err
	}
	return &User{Uid: uid, Name: name, Icon: icon, State: false}, nil
}

func (u *User)LeaveRoom() {
	u.RoomId = 0
	u.State = false
}

//// 连接session
//type Session struct {
//	Uid string
//	Addr *net.UDPAddr
//}

//func (u *User)UpdateSession(addr *net.UDPAddr) {
//
//}