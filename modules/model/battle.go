package model

import (
	"partyanimal-server/framework/job"
	"partyanimal-server/framework/logger"
	"partyanimal-server/framework/net/udp"
	"time"
)

////玩家战斗数据
//type Player User

type Command struct {
	//Uid string			`json:Uid`		// 玩家id
	Cmd string			`json:Cmd`		// 指令数据
	//Time int64			`json:Time`
	//Idx int				`json:Idx`
}

// 帧数据
type Frame struct {
	Index int             	`json:Index` // 帧号
	Cmds []*Command 		`json:Cmds`  // 指令集
}

func NewFrame(index int, cmds []*Command) *Frame {
	return &Frame{Index:index, Cmds:cmds}
}

const ConstMaxSecond = 5*60	// 战斗 最大持续 n分钟
const ConstFramePerSecond = 30	// 每秒帧数
const ConstMaxFrame = ConstFramePerSecond * ConstMaxSecond		// 最大帧数

// 战斗对象数据结构
type Battle struct {
	BattleId string					`json:"BattleId"`		// 战斗唯一id
	Current int						`json:"Current"`		// 战斗当前帧数
	Players []*User					`json:"Players"`		// 参与玩家列表
	Seed int64						`json:"Seed"`		// 随机种子

	// 以下参数不通过对象结构下发
	Frames [ConstMaxFrame]*Frame `json:"-"` // 初始化所有帧
	CmdChannel chan Command      `json:"-"` // 指令传输通道 顺序处理
	Job *job.Job                 `json:"-"` // 帧作业定时器（开始战斗是创建、战斗结束时销毁）
	Room *Room                   `json:"-"` // 回指房间（用于在战斗中访问房间数据和方法）
}

// 执行战斗
func (b *Battle)Run() {
	logger.Battle.Info("battle %s running!!!", b.BattleId)
	if b.Job == nil {
		b.Job = job.NewJob(b.BattleId)
		logger.Battle.Info("battle %s running, Job run every %v", b.BattleId, time.Second/ConstFramePerSecond)
		b.Job.Run(time.Second/ConstFramePerSecond, b.runFrame)		// 30帧
	}
}

// 结束战斗
func (b *Battle)Stop() bool {
	logger.Battle.Info("battle %s stop, current frame %d", b.BattleId, b.Current)
	if b.Job != nil {
		b.Job.Stop()
		b.Job = nil		// 释放job
		logger.Battle.Info("battle %s stop, Job free", b.BattleId)
		return true
	}
	return false
}

// 添加玩家指令
func (b *Battle)AddCmd(uid string, cmd string, time int64, idx int) {
	logger.Battle.Debug("battle %s AddCmd uid = %s, cmd = %s", b.BattleId, uid, cmd)
	//b.CmdChannel<-Command{Uid: uid, Cmd: cmd, Time: time, Idx: idx}
	b.CmdChannel<-Command{Cmd: cmd}

}

// 获取玩家指令
func (b *Battle)getCmd() []*Command{
	var cmds []*Command
	logger.Battle.Debug("battle %s start getCmd", b.BattleId)
	for {
		select {
		case cmd := <-b.CmdChannel:
			logger.Battle.Debug("battle %s getCmd new cmd = %v", b.BattleId, cmd)
			cmds = append(cmds, &cmd)
			break
		default:
			logger.Battle.Debug("battle %s getCmd finish, size = %d", b.BattleId, len(cmds))
			return cmds
			break
		}
	}
}

// 获取指定帧数据
func (b *Battle)GetFrame(index int) []*Frame{
	var result []*Frame
	if index < b.Current && index > 0 {
		frame := b.Frames[index]
		result = append(result, frame)
	}
	return result
}

// 获取当前所有帧数据（用于玩家恢复战斗）
func (b *Battle)GetActiveFrame(index int) []*Frame {
	if index != 0 {
		index = 0
	}
	return b.Frames[:b.Current-1]
}

// 推送下发帧数据结构
type pushFrame struct {
	BattleId string			// 战斗唯一id
	Frames []*Frame			// 帧数据
}

// 推送帧
func (b *Battle)pushFrame() {
	//fmt.Println("start pushFrame")

	var sendFrame []*Frame
	// 发送最新的2帧数据
	if b.Current-3 < 0 {
		sendFrame = b.Frames[:b.Current-1]
	} else {
		sendFrame = b.Frames[b.Current-3:b.Current-1]
	}

	data := pushFrame{BattleId: b.BattleId, Frames: sendFrame}
	for _, user := range b.Players {
		udp.SendTo(user.Addr, data)
	}
	//fmt.Println("end pushFrame")
}

// 执行帧同步
func (b *Battle)runFrame(j *job.Job)  {
	if b.Current >= ConstMaxFrame {
		return
	}
	b.Current++	// 当前帧递增
	logger.Battle.Debug("battle %s runFrame %d", b.BattleId, b.Current)
	newFrame := NewFrame(b.Current,  b.getCmd())		// 创建新一帧数据
	b.Frames[b.Current - 1] = newFrame					// 填充帧
	b.pushFrame() // 向客户端推送帧
	// 帧数达上限 自动停止战斗
	if b.Current >= ConstMaxFrame {
		logger.Battle.Info("battle %s run room.StopBattle, current index %d", b.BattleId, b.Current)
		b.Room.StopBattle()
	}

	// 测试打印
	if b.Current % (ConstFramePerSecond*60) == 0 {
		logger.Battle.Info("battle %s current index %d", b.BattleId, b.Current)
	}
}
