package model

import (
	"partyanimal-server/modules/message"
)

type LoginRequest struct {
	Uid   string        `json:"Uid"`   // 用户唯一id 身份标示
	Sign  string        `json:"Sign"`  // 校验码，由上一次请求返回提供
	MsgId message.MsgId `json:"MsgId"` // 消息id
	Name  string        `json:"Name"`  // 名字
}

type EnterLobbyRequest struct {
	Uid   string        `json:"Uid"`   // 用户唯一id 身份标示
	Sign  string        `json:"Sign"`  // 校验码，由上一次请求返回提供
	MsgId message.MsgId `json:"MsgId"` // 消息id
	Name  string        `json:"Name"`  // 名字
}

type EnterLobbyResponse struct {
	message.ResponseBase
	//RoomMap model.RoomMap	`json:"RoomMap"`
	//Room    *model.Room		`json:"Room"`
	RoomMap RoomMap `json:"RoomMap"`
	Room    *Room   `json:"Room,omitempty"`
}

type UpdateUserRequest struct {
	Uid   string        `json:"Uid"`   	// 用户唯一id 身份标示
	Name  string		`json:"Name"`	// 名字
	Icon  int        `json:"Icon"`  	// 角色
}

type UpdateUserResponse struct {
	message.ResponseBase
}

