package model

import (
	"partyanimal-server/modules/message"
)

type CreateRoomRequest struct {
	message.RequestBase
	Name string `json:"Name"` // 房间名字
}

type CreateRoomResponse struct {
	message.ResponseBase
	Room *Room `json:"Room"`
}

type EnterRoomRequest struct {
	message.RequestBase
	RoomId int			`json:"RoomId"`
}

type EnterRoomResponse struct {
	message.ResponseBase
	Uid string `json:"Uid"`	// 进入房间的玩家id
	Room *Room `json:"Room"`
}

type LeaveRoomRequest struct {
	message.RequestBase
	//RoomId int		`json:"RoomId"`
}

type LeaveRoomResponse struct {
	message.ResponseBase
	Uid string `json:"Uid"`			// 离开房间的玩家id
	Room *Room `json:"Room"`
}

type ReadyRoomRequest struct {
	message.RequestBase
	State bool `json:"State"`
}

type NotifyReadyRoom struct {
	message.NotifyBase
	User *User `json:"User"`
}

type KickRoomRequest struct {
	message.RequestBase
	KickUid string `json:"KickUid"`
}

type NotifyKickRoom struct {
	message.NotifyBase
	KickUid string `json:"KickUid"`
}

type StartBattleRequest struct {
	message.RequestBase
	RoomId int		`json:"RoomId"`
}

type NotifyStartBattle struct {
	message.NotifyBase
	Battle *Battle `json:"Battle"`
}

type NotifyEndBattle struct {
	message.NotifyBase
	Room *Room `json:"Room"`
}

type ReturnBattleRequest struct {
	message.RequestBase
	Index int			`json:"Index"`
}

type ReturnBattleResponse struct {
	message.ResponseBase
	Battle *Battle  `json:"Battle"`
	Frames []*Frame `json:"Frames"`
}


type GetFrameRequest struct {
	message.RequestBase
	Index int			`json:"Index"`
}

type GetFrameResponse struct {
	message.ResponseBase
	Frames []*Frame `json:"Frames"`
}

type AddCommandRequest struct {
	message.RequestBase
	Cmd string
	Time int64
	Idx int
}

type EndBattleRequest struct {
	message.RequestBase
	RoomId int
	Result string
}

type SelectAvatarRequest struct {
	message.RequestBase
	AvatarId int		`json:"AvatarId"`
}

