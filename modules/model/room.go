package model

import (
	"encoding/json"
	"math/rand"
	"partyanimal-server/framework/logger"
	"partyanimal-server/framework/net/udp"
	"partyanimal-server/modules/message"
	"strconv"
	"time"
)

const RoomMemberMax = 12

// 房间状态
type RoomState int
const (
	RoomState_Idle		RoomState = iota		// 空闲
	RoomState_Battle							// 战斗中
)

// 房间对象
type Room struct {
	RoomId int				`json:"RoomId"`
	Name string				`json:"Name"`
	Members []*User			`json:"Members"`
	Battle *Battle			`json:"-"`
	State	RoomState		`json:State`
	CreateTime int64		`json:CreateTime`
	//AvatarMap map[string]string	`json:AvatarMap`		// 玩家选择的角色
}

// 房间集合
type RoomMap map[int]*Room

var index = 1000		// 临时房间号自增 todo 未来数字房间号池
// 新建房间
func NewRoom(master *User, name string) *Room {
	index++
	//fmt.Println("NewRoom", master)
	return &Room{RoomId: index, Name: name, Members: []*User{master}, Battle:nil, State: RoomState_Idle, CreateTime: time.Now().Unix()}
}

func (r *Room)IsMembers(uid string) bool {
	for _, u := range r.Members {
		if u.Uid == uid {
			return true
		}
	}
	return false
}

func (r *Room)IsMaster(uid string) bool {
	return r.Members[0].Uid == uid
}

func (r *Room)IsFull() bool {
	return len(r.Members) >= RoomMemberMax
}

func (r *Room)Join(user *User) bool {
	r.Members = append(r.Members, user)
	user.RoomId = r.RoomId
	return true
}

func (r *Room)ReadyInfo() (int, int) {
	total, ready := 0, 0
	for _, u := range r.Members {
		total++
		if u.State {
			ready++
		}
	}
	return total, ready
}

func (r *Room)IsAllReady() bool {
	for idx, u := range r.Members {
		if idx != 0 && !u.State {
			return false
		}
	}
	return true
}

// 准备 并广播
func (r *Room)Ready(uid string, state bool) bool {
	for _, u := range r.Members {
		if u.Uid == uid {
			if u.State != state {
				u.State = state

				resp := &NotifyReadyRoom{}
				resp.Type = message.Notify_ReadyRoom
				resp.User = u
				r.Broadcast(resp)
			}
			return true
		}
	}
	return false
}

// 清除准备
func (r *Room)ClearReady() bool {
	for _, u := range r.Members {
		u.State = false
	}
	return true
}

// 踢人 并广播
func (r *Room)Kick(uid string) bool {
	for _, u := range r.Members {
		if u.Uid == uid {
			resp := &NotifyKickRoom{}
			resp.Type = message.Notify_KickRoom
			resp.KickUid = uid
			r.Broadcast(resp)
			// 先广播 后踢人（离开房间）
			r.Leave(u)
			return true
		}
	}
	return false
}

func (r *Room)Leave(user *User) bool {
	for i, u := range r.Members {
		if u.Uid == user.Uid {
			r.Members = append(r.Members[:i], r.Members[i+1:]...)	// 从队列删除
			user.LeaveRoom()	// 清除玩家身上的房间号
			return true
		}
	}
	return false
}

func (r *Room)Close() {
	// 清除成员身上的房间号标记
	for _, user := range r.Members {
		user.LeaveRoom()
	}
}

// 是否在战斗中
func (r *Room)IsRunBattle() bool {
	return r.State == RoomState_Battle
}

// 战斗是否结束
func (r *Room)BattleIsOver() bool {
	if r.Battle == nil ||
		(r.Battle != nil && r.Battle.Job == nil) {
		return true
	}
	return false
}

// 开启一场战斗
func (r *Room)RunBattle() {
	// 强制进战斗
	if r.State != RoomState_Battle {
		// 新建战斗对象
		dateStr := time.Now().Format("2006-01-02-15-04-05")
		battleId := dateStr + "--" + strconv.Itoa(r.RoomId)
		r.Battle = &Battle{BattleId: battleId,Current: 0, Players: r.Members, CmdChannel: make(chan Command, 20), Room: r, Seed: rand.Int63n(time.Now().Unix())}
		// 开始战斗
		r.Battle.Run()
		// 更新房间状态
		r.State = RoomState_Battle
		// 推送给房间内所有的人
		resp := &NotifyStartBattle{}
		resp.Type = message.Notify_StartBattle
		resp.Battle = r.Battle
		r.Broadcast(resp)
	}
}

// 结束战斗
func (r *Room)StopBattle() bool {
	if r.State == RoomState_Battle {
		// 推送给房间内所有的人
		r.ClearReady()
		// 结束战斗
		r.Battle.Stop()
		// 更新房间状态
		r.State = RoomState_Idle
		// 释放战斗对象
		r.Battle = nil

		resp := &NotifyEndBattle{}
		resp.Type = message.Notify_EndBattle
		resp.Room = r
		r.Broadcast(resp)

		data,_ := json.Marshal(r)
		logger.Battle.Debug("room states changed %s", data)
		return true
	}
	return false
}

// 房间广播
func (r *Room)Broadcast(data interface{}) {
	success, fail := 0, 0
	for _, user := range r.Members {
		err := udp.SendTo(user.Addr, data)
		if err == nil {
			success++
		} else {
			fail++
		}
	}
	logger.Debug("BroadcastStopBattle total %d, success %d, fail %d", success+fail, success, fail)
}

