package model

import (
	"encoding/json"
	"fmt"
	"net"
	"partyanimal-server/framework/logger"
	"partyanimal-server/modules/db"
)

type Lobby struct {
	Rooms RoomMap		// 房间列表
	Users UserMap		// 玩家列表
}

func NewLobby() *Lobby {
	return &Lobby{Rooms: make(RoomMap), Users: make(UserMap)}
}

// 进入大厅、如果已在房间 返回一个房间
func (l *Lobby)EnterLobby(uid string, name string, addr *net.UDPAddr) (*Room, error) {
	// 内存无数据 则载入
	if l.Users[uid] == nil {
		user, err := LoadUser(uid)
		if err != nil {
			return nil, err
		}
		l.Users[uid] = user
		d,_ := json.Marshal(user)
		logger.Redis.Debug("GetRole data = %v", d)
	}
	// 名字发生变更 存储数据
	if name != "" && name != l.Users[uid].Name {
		l.Users[uid].Name = name
		db.SetRoleName(uid, name) // 更新名字
	}
	l.Users[uid].Addr = addr
	roomId := l.Users[uid].RoomId
	room, ok := l.Rooms[roomId]
	if !ok {
		l.Users[uid].RoomId = 0
	}
	logger.Info("user %s EnterLobby, User size = %d", uid, len(l.Users))
	return room, nil
}

func (l *Lobby)UpdateUser(uid string, name string, icon int) error {
	if l.Users[uid] == nil {
		return fmt.Errorf("user not exist")
	}
	if name != "" && name != l.Users[uid].Name {
		l.Users[uid].Name = name
		db.SetRoleName(uid, name) // 更新名字
	}
	if icon != 0 && icon != l.Users[uid].Icon{
		l.Users[uid].Icon = icon
		db.SetRoleIcon(uid, icon) // 更新形象
	}
	logger.Info("user %s UpdateUser, name = %s, icon = %d", uid, name, icon)
	return nil
}

func (l *Lobby)GetRoom(uid string) {
	//logger.Debug("after UpdateUser, User size = %d", len(l.Users))
}

func (l *Lobby)CreateRoom(uid string, name string) (*Room, error) {
	if l.Users[uid] == nil {
		return nil, fmt.Errorf("user not exist")
	}
	_, ok := l.Rooms[l.Users[uid].RoomId]
	if ok {
		return nil, fmt.Errorf("user in the room, cant create new room")
	}

	room := NewRoom(l.Users[uid], name)
	l.Rooms[room.RoomId] = room
	l.Users[uid].RoomId = room.RoomId
	logger.Info("after CreateRoom %d, Room size = %d",  room.RoomId, len(l.Rooms))
	return room, nil
}

func (l *Lobby)EnterRoom(uid string, roomId int) (*Room, error) {
	if l.Users[uid] == nil {
		return nil, fmt.Errorf("user not exist")
	}
	room := l.Rooms[roomId]
	if room == nil {
		return nil, fmt.Errorf("room not exist")
	}
	// 已经是成员了
	if room.IsMembers(uid) {
		return room, nil
	}

	if room.IsFull() {
		return nil, fmt.Errorf("room is full")
	}
	if room.State != RoomState_Idle {
		return nil, fmt.Errorf("cant enter this room")
	}
	// 添加进房间成员列表
	room.Join(l.Users[uid])
	logger.Info("after EnterRoom, Room %d member size = %d", room.RoomId, len(room.Members))
	return room, nil
}

func (l *Lobby)ReadyRoom(uid string, state bool) error {
	logger.Debug("ReadyRoom %s %t", uid, state)
	if l.Users[uid] == nil {
		return fmt.Errorf("user not exist")
	}
	if l.Users[uid].RoomId == 0 {
		return fmt.Errorf("user not in room")
	}
	room := l.Rooms[l.Users[uid].RoomId]
	if room == nil {
		return fmt.Errorf("room not exist")
	}
	// 战斗中 不可操作
	if room.State == RoomState_Battle {
		return fmt.Errorf("room in battle, cant ready")
	}
	// 已经不是成员了
	if !room.IsMembers(uid) {
		return nil
	}

	room.Ready(uid, state)
	total, ready := room.ReadyInfo()
	logger.Info("after ReadyRoom, Room %d ready = %d/%d", room.RoomId, ready, total)
	return nil
}

func (l *Lobby)KickRoom(uid string, kickUid string) error {
	logger.Debug("KickRoom %s", uid)
	if l.Users[uid] == nil {
		return fmt.Errorf("user not exist")
	}
	if l.Users[uid].RoomId == 0 {
		return fmt.Errorf("user not in room")
	}
	room := l.Rooms[l.Users[uid].RoomId]
	if room == nil {
		return fmt.Errorf("room not exist")
	}
	// 战斗中 不可操作
	if room.State == RoomState_Battle {
		return fmt.Errorf("room in battle, cant kick")
	}
	// 只有房主有权限踢人
	if !room.IsMaster(uid) {
		return fmt.Errorf("you are not master, cant kick")
	}

	if !room.Kick(kickUid) {
		return fmt.Errorf("member not find, cant kick")
	}
	logger.Info("after KickRoom, Room %d member size = %d", room.RoomId, len(room.Members))
	return nil
}

func (l *Lobby)LeaveRoom(uid string) (*Room, error) {
	if l.Users[uid] == nil {
		return nil, fmt.Errorf("user not exist")
	}
	if l.Users[uid].RoomId == 0 {
		return nil, fmt.Errorf("user not in room")
	}
	room := l.Rooms[l.Users[uid].RoomId]
	if room == nil {
		return nil, fmt.Errorf("room not exist")
	}
	// 战斗中 不可退出
	if room.State == RoomState_Battle {
		return nil, fmt.Errorf("room in battle, cant leave")
	}
	// 已经不是成员了
	if !room.IsMembers(uid) {
		return nil, nil
	}
	// 如果是房组 不可离开
	if room.IsMaster(uid) {
		return nil, fmt.Errorf("master cant leave room")
	}
	// 离开房间
	room.Leave(l.Users[uid])
	logger.Info("after LeaveRoom, Room %d member size = %d", room.RoomId, len(room.Members))
	return room, nil
}

func (l *Lobby)CloseRoom(uid string) (*Room, error) {
	if l.Users[uid] == nil {
		return nil, fmt.Errorf("user not exist")
	}
	if l.Users[uid].RoomId == 0 {
		return nil, fmt.Errorf("user not in room")
	}
	room := l.Rooms[l.Users[uid].RoomId]
	if room == nil {
		return nil, fmt.Errorf("room not exist")
	}
	// 战斗中 不可退出
	if room.State == RoomState_Battle {
		return nil, fmt.Errorf("room in battle, cant leave")
	}
	if !room.IsMaster(uid) {
		return nil, fmt.Errorf("you are not master, cant close room")
	}
	room.Close()
	delete(l.Rooms, room.RoomId)
	logger.Info("after CloseRoom %d, Room size = %d",  room.RoomId, len(l.Rooms))
	return room, nil
}

