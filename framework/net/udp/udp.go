package udp

import (
	"encoding/json"
	"fmt"
	"net"
	"partyanimal-server/framework/logger"
)

type Session struct {
	Conn *net.UDPConn
	RemoteAddr *net.UDPAddr
	//LastError error
	//Handler []interface{}
}
// 回调方法
type Handler func(service *Session, data []byte, len int)

var maxLen = 0

func Listen(address string, cb Handler) error {
	udpAddr, err := net.ResolveUDPAddr("udp4", address)
	if err != nil {
		logger.Error("udp net.ResolveUDPAddr err = %v", err)
		logger.Net.Error("udp net.ResolveUDPAddr err = %v", err)

		//return &UDPSession{UDPConn: nil, LastError: err}
		return err
	}
	socket, err := net.ListenUDP("udp4", udpAddr)
	if err != nil {
		logger.Error("udp net.ListenUDP err = %v", err)
		logger.Net.Error("udp net.ListenUDP err = %v", err)

		//return &UDPSession{UDPConn: nil, LastError: err}
		return err
	}
	logger.Net.Info("udp net.ListenUDP start")
	defer func() {
		logger.Net.Info("udp net.ListenUDP close")
		socket.Close()
	}()
	for {
		// 读取数据
		data := make([]byte, 1024)
		len, remoteAddr, err := socket.ReadFromUDP(data)
		if err != nil {
			logger.Error("udp.Listen ReadFromUDP err = %v", err)
			logger.Net.Error("udp.Listen ReadFromUDP err = %v", err)
			continue
		}
		//fmt.Println("len =", len, "remoteAddr =", remoteAddr)
		//fmt.Println("data", data)
		//socket.WriteToUDP([]byte("test"), remoteAddr)
		cb(&Session{Conn: socket, RemoteAddr: remoteAddr}, data, len)
		data = nil
	}
	//return &UDPSession{UDPConn: udpConn, LastError: nil}
}

func (s *Session)Send(v interface{}) error {
	//socket, err := net.DialUDP("udp4", nil, s.RemoteAddr)
	//if err != nil {
	//	return err
	//}
	data, err := json.Marshal(v)
	if err != nil {
		logger.Error("udp.Session json.Marshal err = %v", err)
		logger.Net.Error("udp.Session json.Marshal err = %v", err)

		return err
	}

	if len(data) > maxLen {
		maxLen = len(data)
		logger.Net.Info("udp.Session Send data refresh maxLen = %d to %v", len(data), s.RemoteAddr)
	}

	logger.Net.Debug("udp.Session Send data = %s to %v", string(data), s.RemoteAddr)

	s.Conn.WriteToUDP(data, s.RemoteAddr)
	return nil
}

func SendTo(addr *net.UDPAddr, v interface{}) error {
	if addr == nil {
		logger.Error("udp.SendTo addr missing")
		logger.Net.Error("udp.SendTo addr missing")

		return fmt.Errorf("addr missing")
	}
	data, err := json.Marshal(v)
	if err != nil {
		logger.Error("udp.SendTo err = %v", err)
		logger.Net.Error("udp.SendTo err = %v", err)

		return err
	}

	socket, err := net.DialUDP("udp4", nil, addr)
	if err != nil {
		return err
	}
	socket.Write(data)

	if len(data) > maxLen {
		maxLen = len(data)
		//logger.Net.Info("udp.Session Send data maxLen = %d to %v", len(data), addr)
		logger.Net.Info("udp.SendTo send data = %s refresh maxLen = %d to %v", string(data), len(data), addr)
	}

	logger.Net.Debug("udp.SendTo send data = %s to %v", string(data), addr)
	return nil
}