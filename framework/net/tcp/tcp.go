package tcp

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"partyanimal-server/framework/logger"
)

type Session struct {
	Conn *net.TCPConn
}
// 回调方法
type Handler func(service *Session, data []byte, len int)

func Listen(address string, cb Handler) error {
	tcpAddr, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		logger.Error("tcp net.ResolveTCPAddr err = %v", err)
		logger.Net.Error("tcp net.ResolveTCPAddr err = %v", err)

		//return &UDPSession{UDPConn: nil, LastError: err}
		return err
	}
	socket, err := net.ListenTCP("tcp4", tcpAddr)
	if err != nil {
		logger.Error("tcp net.ListenTCP err = %v", err)
		logger.Net.Error("tcp net.ListenTCP err = %v", err)
		return err
	}
	defer func() {
		socket.Close()
		logger.Net.Info("tcp net.ListenTCP close")
	}()
	logger.Net.Info("tcp net.ListenTCP start")

	for{
		tcpConn,err := socket.AcceptTCP()
		if err!=nil {
			fmt.Println(err)
			continue
		}
		logger.Net.Debug("tcp AcceptTCP: A client connected : %s" , tcpConn.RemoteAddr().String())
		go tcpPipe(tcpConn, cb)
	}
	//return &UDPSession{UDPConn: udpConn, LastError: nil}
}

func tcpPipe(conn *net.TCPConn, cb Handler){
	reader := bufio.NewReader(conn)
	for {
		data := make([]byte, 1024)
		len, err := reader.Read(data)
		if err!=nil || err == io.EOF {
			break
		}
		logger.Net.Debug("tcp Read data : %s", string(data))

		cb(&Session{Conn: conn}, data, len)
	}
}

var maxLen = 0

func (s *Session)Send(v interface{}) error {
	ipStr := s.Conn.RemoteAddr().String()
	defer func() {
		logger.Net.Debug("tcp Disconnected : %s", ipStr)
		s.Conn.Close()
	}()

	data, err := json.Marshal(v)
	if err != nil {
		logger.Error("tcp Send json.Marshal err = %v", err)
		logger.Net.Error("tcp Send json.Marshal err = %v", err)

		return err
	}

	size := int32(len(data))
	var sizeByte = make([]byte, 4)
	sizeByte[0] = uint8(size)
	sizeByte[1] = uint8(size >> 8)
	sizeByte[2] = uint8(size >> 16)
	sizeByte[3] = uint8(size >> 24)

	var buffer bytes.Buffer
	buffer.Write(sizeByte)
	buffer.Write(data)
	//logger.Net.Debug("0", sizeByte[0])
	//logger.Net.Debug("1", sizeByte[1])
	//logger.Net.Debug("2", sizeByte[2])
	//logger.Net.Debug("3", sizeByte[3])

	//logger.Net.Debug("tcp Send data = %s", string(data))
	//logger.Net.Debug("tcp Send data.length = %d", len(data))
	//logger.Net.Debug("tcp Send buffer.length = %d", buffer.Len())
	if len(data) > maxLen {
		maxLen = len(data)
		//logger.Net.Info("udp.Session Send data maxLen = %d to %v", len(data), addr)
		logger.Net.Info("tcp.Send refresh maxLen = %d", len(data))
	}
	s.Conn.Write(buffer.Bytes())

	return nil
}