package file

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/toolkits/file"
	"os"
)

// LoadJsonConfiguration load config from json file
func LoadJsonConfiguration(filename string, v interface{}) {
	dst := new(bytes.Buffer)
	var (
		content string
	)
	err := json.Compact(dst, []byte(filename))

	if err != nil {
		content, err = ReadFile(filename)
		if err != nil {
			fmt.Fprintf(os.Stderr, "LoadJsonConfiguration: Error: Could not read %q: %s\n", filename, err)
			os.Exit(1)
		}
	} else {
		content = string(dst.Bytes())
	}

	err = json.Unmarshal([]byte(content), v)
	if err != nil {
		fmt.Fprintf(os.Stderr, "LoadJsonConfiguration: Error: Could not parse json configuration in %q: %s\n", filename, err)
		os.Exit(1)
	}
}

func ReadFile(path string) (string, error) {
	if path == "" {
		return "", fmt.Errorf("[%s] path empty", path)
	}

	if !file.IsExist(path) {
		return "", fmt.Errorf("config file %s is not exist", path)
	}

	configContent, err := file.ToTrimString(path)
	if err != nil {
		return "", fmt.Errorf("read file %s fail %s", path, err)
	}

	return configContent, nil
}

func IsExist(path string) bool {
	return file.IsExist(path)
}
