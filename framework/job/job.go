package job

import (
	"partyanimal-server/framework/logger"
	"time"
)

type TickFunc func(j *Job)

type Job struct {
	id     string
	jf     TickFunc
	params map[string]interface{}
	ch     chan int
}

func NewJob(id string) *Job {
	return &Job{
		id:		id,
		params: make(map[string]interface{}),
		ch:     make(chan int, 1),
	}
}

// 执行job 间隔时间t
func (j *Job)Run(t time.Duration, jf TickFunc) {
	logger.Job.Info("job %s start", j.id)
	j.jf = jf
	ticker := time.NewTicker(t)
	go func() {
		defer func(){
			logger.Job.Info("job %s goroutine exit", j.id)
		}()
		for {
			select {
			case <-ticker.C:
				j.jf(j)
				break
			case <-j.ch:
				logger.Job.Info("job %s stopped", j.id)
				ticker.Stop()
				return
			}
		}
	}()
}

func (j *Job)Stop() {
	logger.Job.Info("job %s stop", j.id)
	j.ch <- 0
	close(j.ch)
}