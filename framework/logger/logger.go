package logger

import (
	"github.com/jeanphorn/log4go"
)

type logFunc func(arg0 interface{}, args ...interface{})

// Base
var (
	Info  logFunc
	Debug logFunc
	Warn  logFunc
	Error logFunc
)

var Battle *log4go.Filter
var Net *log4go.Filter
var Job *log4go.Filter
var Redis *log4go.Filter

func init() {
	log4go.LoadConfiguration("config/log4go.json")
	// 全局filter
	Debug = log4go.LOGGER("Debug").Debug
	Info = log4go.LOGGER("Info").Info
	Warn = log4go.LOGGER("Warn").Warn
	Error = log4go.LOGGER("Error").Error

	// 独立模块filter
	Net = log4go.LOGGER("Net") 	// 网络模块
	Job = log4go.LOGGER("Job")		// 定时任务
	Battle = log4go.LOGGER("Battle")	// 战斗
	Redis = log4go.LOGGER("Redis")	// Redis

	Info("logger init success .")
}
