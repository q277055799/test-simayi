package redis

func HSet(k string, f string, v interface{}) error {
	_, err := Client().HSet(ctx, k, f, v).Result()
	return err
}

func HGet(k string, f string) (interface{}, error) {
	return Client().HGet(ctx, k, f).Result()
}

func HMGet(k string, f []string) ([]interface{}, error) {
	return Client().HMGet(ctx, k, f...).Result()
}
