/* 对value值进行json序列化和json反序列化 redis get和set方法
 */
package redis

import (
	"encoding/json"
	"github.com/go-redis/redis"
	"partyanimal-server/framework/logger"
)

func HSetJson(k string, f string, v interface{}) error {
	// json序列化s
	//val, ok := v.(struct{})
	_v, err := json.Marshal(v)
	if err != nil {
		return err
	}
	logger.Redis.Debug("HSet", k, f, _v)

	_, err = Client().HSet(ctx, k, f, _v).Result()
	if err != nil {
		return err
	}
	return nil
}

func HGetJson(k string, f string, v interface{}) error {
	_v, err := Client().HGet(ctx, k, f).Result()
	// 无数据
	if err == redis.Nil {
		return nil
	}
	// 访问出错 返回错误
	if err != nil {
		return err
	}
	// 解析数据并返回解析结果
	return json.Unmarshal([]byte(_v), v)
}

func HMGetJson(k string, f []string, v []interface{}) error {
	vals, err := Client().HMGet(ctx, k, f...).Result()
	if err != nil {
		return err
	}
	for _, val := range vals {
		s, ok := val.(string)
		if ok {
			var _v map[string]interface{}
			json.Unmarshal([]byte(s), _v)
			v = append(v, _v)
		} else {
			v = append(v, nil)
		}
	}
	return nil
}

