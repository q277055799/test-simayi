package redis

import (
	"context"
	"github.com/go-redis/redis"
	"partyanimal-server/framework/file"
	"partyanimal-server/framework/logger"
)

type ClusterConfig struct {
	Addrs    []string `json:"address"`
	Password string   `json:"password"`
}

var ctx = context.Background()
var clusterClient *ClusterClient

// 通过组合来继承
type ClusterClient struct {
	redis.ClusterClient
}

// 集群模式初始化
func init() {
	var cfg ClusterConfig
	file.LoadJsonConfiguration("config/redisCluster.json", &cfg)

	client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:    cfg.Addrs,
		Password: cfg.Password,
	})

	clusterClient = &ClusterClient{*client}

	cmd := clusterClient.Ping(ctx)
	logger.Redis.Info("redis cluster init success, %s", cmd)
}

func Client() (c *ClusterClient) {
	return clusterClient
}
