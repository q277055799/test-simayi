package handler

import (
	"partyanimal-server/modules"
	"partyanimal-server/modules/message"
	"partyanimal-server/modules/model"
)

func (this *DoHandler)SelectAvatar()  {
	msg := model.SelectAvatarRequest{}
	this.ParseMsg(&msg)

	// 有没有玩家数据
	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}
	// 已经在战斗中 不可调整
	if room.IsRunBattle() {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_CantChangeAvatarInBattle})
		return
	}


}

func (this *DoHandler)StartBattle() {
	msg := model.StartBattleRequest{}
	this.ParseMsg(&msg)

	// 有没有玩家数据
	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}

	// 不是房主
	if !room.IsMaster(msg.Uid) {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotRoomMaster})
		return
	}

	// 已经在战斗中 返回个人
	if room.IsRunBattle() {
		resp := &model.NotifyStartBattle{}
		resp.Type = message.Notify_StartBattle
		resp.Battle = room.Battle
		this.Srv.Send(resp)
		return
	}

	if !room.IsAllReady() {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAllReady})
		return
	}

	// 开启一场战斗
	room.RunBattle()
}

func (this *DoTCPHandler)ReturnBattle() {
	msg := model.ReturnBattleRequest{}
	this.ParseMsg(&msg)

	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}
	if room.State == model.RoomState_Idle {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_BattleIsOver})
		return
	}

	frames := room.Battle.GetActiveFrame(msg.Index)
	resp := &model.ReturnBattleResponse{}
	resp.Code = message.CODE_OK
	resp.Battle = room.Battle
	resp.Frames = frames
	this.Srv.Send(resp)
}

func (this *DoHandler)GetFrame() {
	msg := model.GetFrameRequest{}
	this.ParseMsg(&msg)

	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}
	if room.State == model.RoomState_Idle {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_BattleIsOver})
		return
	}
	frames := room.Battle.GetFrame(msg.Index)
	resp := &model.GetFrameResponse{}
	resp.Code = message.CODE_OK
	resp.Frames = frames
	this.Srv.Send(resp)
}

func (this *DoHandler)AddCommand() {
	msg := model.AddCommandRequest{}
	this.ParseMsg(&msg)

	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}
	if room.State == model.RoomState_Idle {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_BattleIsOver})
		return
	}
	room.Battle.AddCmd(msg.Uid, msg.Cmd, msg.Time, msg.Idx)
	this.Srv.Send(&message.ResponseBase{Code: message.CODE_OK})
}

// 结束战斗
func (this *DoHandler)EndBattle() {
	msg := model.EndBattleRequest{}
	this.ParseMsg(&msg)

	user, ok := modules.Lobby.Users[msg.Uid]
	if !ok {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_ShouldEnterLobbyFirst})
		return
	}
	// 没有房间
	room, ok := modules.Lobby.Rooms[user.RoomId]
	if !ok{
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_NotAtRoom})
		return
	}
	// 停止战斗、首次调用会停止战斗 并发送广播，非首次 只通知自己
	do := room.StopBattle()
	if !do {
		// 通知自己
		resp := &message.NotifyBase{}
		resp.Type = message.Notify_EndBattle
		this.Srv.Send(resp)
	}
}