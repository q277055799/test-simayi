package handler

import (
	"encoding/json"
	"partyanimal-server/framework/logger"
	"partyanimal-server/framework/net/tcp"
	"partyanimal-server/framework/net/udp"
	"partyanimal-server/modules"
	"partyanimal-server/modules/message"
	"time"
)

type DoHandler struct {
	Srv   *udp.Session
	MsgId message.MsgId
	Msg   []byte
	//Time int64
}

func (this *DoHandler) ParseMsg(v interface{})  {
	json.Unmarshal(this.Msg, v)
}

// 心跳包处理 更新客户端session
func (this *DoHandler)HeartBeat()  {
	uid := string(this.Msg)
	//fmt.Println("run HeartBeat", uid)

	user, ok := modules.Lobby.Users[uid]
	if ok {
		// 依据心跳包 更新客户端地址
		user.Addr = this.Srv.RemoteAddr
		user.LastOnlineTime = time.Now().Unix()*1000
		//fmt.Println("do HeartBeat", user.Addr)
	}
}

func (this *DoHandler) do() {

	switch this.MsgId {
	case message.MSG_Heartbeat:
		this.HeartBeat()
		break
	case message.MSG_EnterLobby:
		this.EnterLobby()
		break
	case message.MSG_CreateRoom:
		this.CreateRoom()
		break
	case message.MSG_EnterRoom:
		this.EnterRoom()
		break
	case message.MSG_LeaveRoom:
		this.LeaveRoom()
		break
	case message.MSG_CloseRoom:
		this.CloseRoom()
		break
	case message.MSG_StartBattle:
		this.StartBattle()
		break
	case message.MSG_AddCmd:
		this.AddCommand()
		break
	case message.MSG_GetFrame:
		this.GetFrame()
		break
	case message.MSG_EndBattle:
		this.EndBattle()
		break
	case message.MSG_UpdateUser:
		this.UpdateUser()
		break
	case message.MSG_ReadyRoom:
		this.ReadyRoom()
		break
	case message.MSG_KickRoom:
		this.KickRoom()
		break
	default:
		logger.Error("error msgId = %s", this.MsgId)
		this.Srv.Send(message.ResponseBase{Code: message.CODE_ParamError})
	}
}

func Handler(srv *udp.Session, data []byte, len int) {
	msgId := message.MsgId(data[0])
	//var t int64 = 0
	//t = int64(data[1]) + int64(data[2]) << 8 + int64(data[3]) << 16 + int64(data[4]) << 24 +
	//	int64(data[5]) << 32 + int64(data[6]) << 40 + int64(data[7]) << 48 + int64(data[8]) << 56
	//logger.Debug("before %d", t)
	//t = time.Now().UnixNano()/1000 - t
	//logger.Debug("after %d", t)

	msg := data[1:len]
	if msgId != 0 {
		//logger.Debug("Run Handler from %v msgid = %v msg = %s t = %d", srv.RemoteAddr, msgId, string(msg), t)
		logger.Debug("Run Handler from %v msgid = %v msg = %s", srv.RemoteAddr, msgId, string(msg))
	}
	DH := DoHandler{Srv: srv, MsgId: msgId, Msg: msg}
	DH.do()
}

type DoTCPHandler struct {
	Srv   *tcp.Session
	MsgId message.MsgId
	Msg   []byte
}

func (this *DoTCPHandler) ParseMsg(v interface{})  {
	json.Unmarshal(this.Msg, v)
}

func (this *DoTCPHandler) do() {

	switch this.MsgId {
	case message.MSG_ReturnBattle:
		this.ReturnBattle()
		break
	default:
		logger.Error("error msgId = %s", this.MsgId)
		this.Srv.Send(message.ResponseBase{Code: message.CODE_ParamError})
	}
}


func TCPHandler(srv *tcp.Session, data []byte, len int) {
	msgId := message.MsgId(data[0])
	msg := data[1:len]
	if msgId != 0 {
		logger.Debug("Run tcp Handler from %v msgid = %v msg = %s", srv.Conn.RemoteAddr(), msgId, string(msg))
	}
	DH := DoTCPHandler{Srv: srv, MsgId: msgId, Msg: msg}
	DH.do()
}
