package handler

import (
	"partyanimal-server/modules"
	"partyanimal-server/modules/message"
	"partyanimal-server/modules/model"
)

func (this *DoHandler)Login() {

}

func (this *DoHandler)EnterLobby() {
	msg := model.EnterLobbyRequest{}
	this.ParseMsg(&msg)
	//json.Unmarshal(this.Msg, &msg)
	room, err := modules.Lobby.EnterLobby(msg.Uid, msg.Name, this.Srv.RemoteAddr)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail})
		return
	}

	resp := &model.EnterLobbyResponse{}
	resp.Code = message.CODE_OK
	if room != nil {
		resp.Room = room
	} else {
		resp.RoomMap = modules.Lobby.Rooms
	}
	this.Srv.Send(resp)
}

func (this *DoHandler)CreateRoom() {
	msg := model.CreateRoomRequest{}
	this.ParseMsg(&msg)
	room, err := modules.Lobby.CreateRoom(msg.Uid, msg.Name)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail})
		return
	}

	resp := &model.CreateRoomResponse{}
	resp.Code = message.CODE_OK
	resp.Room = room
	this.Srv.Send(resp)
}

func (this *DoHandler)EnterRoom() {
	msg := model.EnterRoomRequest{}
	this.ParseMsg(&msg)
	room, err := modules.Lobby.EnterRoom(msg.Uid, msg.RoomId)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail, Msg: err.Error()})
		return
	}

	resp := &model.EnterRoomResponse{}
	resp.Code = message.CODE_OK
	resp.Uid = msg.Uid
	resp.Room = room
	room.Broadcast(resp)
	//this.Srv.Send(resp)
}

// 战前准备
func (this *DoHandler)ReadyRoom() {
	msg := model.ReadyRoomRequest{}
	this.ParseMsg(&msg)

	err := modules.Lobby.ReadyRoom(msg.Uid, msg.State)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail, Msg: err.Error()})
		return
	}
}

// 踢人
func (this *DoHandler)KickRoom() {
	msg := model.KickRoomRequest{}
	this.ParseMsg(&msg)

	err := modules.Lobby.KickRoom(msg.Uid, msg.KickUid)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail, Msg: err.Error()})
		return
	}
}

func (this *DoHandler)LeaveRoom() {
	msg := model.LeaveRoomRequest{}
	this.ParseMsg(&msg)
	room, err := modules.Lobby.LeaveRoom(msg.Uid)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail, Msg: err.Error()})
		return
	}

	notify := &message.NotifyBase{}
	notify.Type = message.Notify_LeaveRoom
	notify.Data = msg.Uid
	// 给自己发送
	this.Srv.Send(notify)
	if room != nil {
		// 给房间其他人发送
		room.Broadcast(notify)
	}

}

func (this *DoHandler)CloseRoom() {
	msg := model.LeaveRoomRequest{}
	this.ParseMsg(&msg)
	room, err := modules.Lobby.CloseRoom(msg.Uid)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail, Msg: err.Error()})
		return
	}

	// 给房间所有人发送
	notify := &message.NotifyBase{}
	notify.Type = message.Notify_CloseRoom
	room.Broadcast(notify)
}

// 更新玩家信息
func (this *DoHandler)UpdateUser() {
	msg := model.UpdateUserRequest{}
	this.ParseMsg(&msg)

	err := modules.Lobby.UpdateUser(msg.Uid, msg.Name, msg.Icon)
	if err != nil {
		this.Srv.Send(&message.ResponseBase{Code: message.CODE_Fail})
		return
	}

	resp := &message.ResponseBase{}
	resp.Code = message.CODE_OK
	this.Srv.Send(resp)
}